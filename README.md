### SEQUENCE DIAGRAM
![Alt text](Sequence_PaymentKIT.svg)

#### AUTHENTICATION, GET TOKEN FLOW
1.`/token` **[POST]** API call with 
```
{
    API_KEY,
    GATEWAY_TYPE
    Merchant_Details, (specific to gateway)
}
```
2.PaymentKit responds with JWT Token and trace_id
```
{
    token (embeded with merchant_details, expiration time, trace_id),
    trace_id (basically uuid for tracking client actions with this token)
}
```
`NOTE`: we use trace_id in every logging we do to track all actions
token is embedded with trace_id so token will be valid if it is used with associated trace_id

<hr/>

#### INITIATING PAYMENT FLOW
a.1. `/payment/initiate` **[POST]** API call with
```
{
    token,
    trace_id,
    transaction_params (gateway specific params for initiating transaction),
}
```
a.2. decrypts token and calls payment gateway API with 
```
{
    merchant_details,
    transaction_params
}
```
a.3. response from payment gateways
`Example` transaction_params for TRIPOS: 
```
{
    transaction_amount,
    return_url,
    reference_number (order_id)
}
``` 

<hr/>

#### CAPTURE PAYMENT FLOW
b.1. `/payment/capture` **[POST]** API call with
```
{
    token,
    trace_id,
    capture_params (gateway specific params for capturing transaction),
}
```
b.2. decrypts token and calls payment gateway API with 
```
{
    merchant_details,
    capture_params
}
```
b.3. response from payment gateways
`Example` capture_params for TRIPOS: 
```
{
    transaction_amount,
    reference_number,(order_id)
    transaction_id
}
``` 

<hr/>

#### VOID PAYMENT FLOW
c.1. /payment/void **[POST]** API call with
```
{
    token,
    trace_id,
    void_params (gateway specific params for voiding transaction),
}
```
c.2. decrypts token and calls payment gateway API with 
```
{
    merchant_details,
    void_params
}
```
c.3. response from payment gateways
Example void_params for TRIPOS: 
```
{
    transaction_setup_id,
    transaction_id,
    transaction_amount,
    reference_number,
}
``` 

<hr/>

#### STATUS CHECK PAYMENT FLOW
d.1. /payment/status **[GET]** API call with
```
{
    token,
    trace_id,
    status_params (gateway specific params for status of transaction),
}
```
d.2. decrypts token and calls payment gateway API with 
```
{
    merchant_details,
    status_params
}
```
d.3. response from payment gateways
`Example` status_params for TRIPOS: 
```
{
    transaction_setup_id
}
``` 
