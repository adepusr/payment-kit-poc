import functools
import datetime
import traceback
import uuid
from logging.config import dictConfig
import requests

from gevent import pywsgi
import jwt
import http
from flask import Flask, request

from importlib import import_module

APP_NAME = 'Payment-Kit'
APP_KEY = '5839fbd3-90ab-4cae-b85a-cb249ad5eb92'
ALGORITHM = 'HS256'

PAYMENT_ERROR_MSG = 'Payment is not completed: {}'
REFUND_ERROR_MSG = 'Refund is not completed: {}'
PAYMENT_ESTABLISHMENT_CONFIGURATION_ERROR_MSG = 'This establishment does not support Web ordering.'
SECURITY_CODE_INVALID_ERROR_MSG = 'Your security code is not valid.'
PAYMENT_NOT_PROCESSED_ERROR_MSG = 'We were not able to process this payment at the moment. Please try again later.'
PAYMENT_NOT_SUCCESSFUL_ERROR_MSG = 'Payment not successful.'


APP = Flask(APP_NAME)
LOGGER = APP.logger

dictConfig({
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'default': {
            'format': '[%(asctime)s] %(levelname)s in %(module)s: %(message)s',
        }
    },
    'handlers': {
        'wsgi': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'stream': 'ext://sys.stdout',
            'formatter': 'default'
        }
    },
    'loggers': {
        APP_NAME: {
            'level': 'INFO',
            'handlers': ['wsgi'],
            'propagate': False
        }
    }
})

# decoded_jwt = jwt.decode(encoded_jwt, APP_KEY, algorithms=[ALGORITHM])


def log_request(func):
    """
    Log basic information about a request, then proceed with the request.
    Useful if you've turned off built-in Werkzeug routing.
    """
    @functools.wraps(func)
    def decorated_function(*args, **kwargs):
        path = request.path
        method = request.method
        access_route = request.access_route
        LOGGER.info(f'Request: Route "{",".join(access_route)}" -> "{method}" {path}')
        return func(*args, **kwargs)
    return decorated_function


def authorize_domocli_request(func):
    """
    Protect requests to domocli; short-circuit and return unauthorized if improper auth.
    """
    @functools.wraps(func)
    def decorated_function(*args, **kwargs):
        # basic protection while testing
        # should implement better auth scheme if this will be public app in prod
        x_payment_kit_api_key = request.headers.get('x-payment-kit-api-key')
        payment_kit_session_id = request.headers.get('x-payment-kit-session-id')
        if x_payment_kit_api_key != APP_KEY:
            LOGGER.info(f'Unauthorized')
            return {
                'statusCode': http.HTTPStatus.UNAUTHORIZED,
                'version': 1,
                'url': request.base_url,
                'data': {
                    'errorMessage': 'Not authorized to call this service'
                },
            }, http.HTTPStatus.UNAUTHORIZED
        # if not payment_kit_session_id:
        #     return {
        #         'statusCode': http.HTTPStatus.BAD_REQUEST,
        #         'version': 1,
        #         'url': request.base_url,
        #         'data': {
        #             'errorMessage': 'x-payment-kit-session-id is not present in the request'
        #         }
        #     }, http.HTTPStatus.BAD_REQUEST
        return func(*args, **kwargs)
    return decorated_function


@APP.route('/token', methods=['POST'])
@log_request
@authorize_domocli_request
def create_token():
    """
    'headers': {
        'x-payment-kit-api-key':
        'x-payment-kit-app-name':
        'x-payment-kit-session-id':
    },
    'json': {
        'gateway': 'TRIPOS'
        'details': {
            'transaction_url': '',
            'account_id': ''
            'account_token': ''
            'acceptor_id': ''
            'application_id': ''
            'test_mode': ''
        }
    }
    """
    # TODO: schema validation as per gatewayType.
    created_at = datetime.datetime.utcnow()
    expires_at = created_at + datetime.timedelta(seconds=300)
    x_payment_kit_session = str(uuid.uuid4())
    request.json['exp'] = expires_at
    request.json['iss'] = 'revel-payment-kit'
    request.json['aud'] = [x_payment_kit_session]
    response = {
        'statusCode': http.HTTPStatus.CREATED,
        'version': 1,
        'x-payment-kit-session-id': x_payment_kit_session,
        'url': request.base_url,
        'data': {
            'token': jwt.encode(request.json, APP_KEY, algorithm=ALGORITHM).decode("utf-8"),
            'type': 'Bearer',
            'createdAt': str(created_at),
            'expiresAt': str(expires_at),
        }
    }
    return response, http.HTTPStatus.CREATED


@APP.route('/start_payment', methods=['POST'])
@log_request
@authorize_domocli_request
def payment():
    """
    {

    }
    """
    # TODO: schema validation as per gatewayType.
    try:
        decoded = jwt.decode(
            request.headers.get('token'),
            APP_KEY,
            audience=request.headers.get('x-payment-kit-session-id'),
            algorithms=[ALGORITHM]
        )
        payment_mod = import_module(f'payments.{(decoded.get("gateway")).lower()}.service')
        responses = payment_mod.start_payment(decoded, request.json)
        print(responses)
        response = {
            'statusCode': http.HTTPStatus.CREATED,
            'version': 1,
            'url': request.base_url,
            'data': {
                'decoded': decoded
            }
        }

    except (jwt.exceptions.ExpiredSignatureError, jwt.exceptions.InvalidAudienceError) as err:
        return {
            'statusCode': http.HTTPStatus.BAD_REQUEST,
            'version': 1,
            'url': request.base_url,
            'data': {
                'errorMessage': str(err)
            }
        }, http.HTTPStatus.BAD_REQUEST
    return response, http.HTTPStatus.CREATED


def capture():
    decoded = jwt.decode(
        request.headers.get('token'),
        APP_KEY,
        audience=request.headers.get('x-payment-kit-session-id'),
        algorithms=[ALGORITHM]
    )
    payment_mod = import_module(f'payments.{(decoded.get("gateway")).lower()}.service')
    responses = payment_mod.capture_transaction(decoded, request.json)
    print(responses)


if __name__ == '__main__':
    LOGGER.info('starting app server...')
    pywsgi.WSGIServer(('', 5000), APP, log=None).serve_forever()
