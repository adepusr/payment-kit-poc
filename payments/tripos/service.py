import logging
import os
import traceback
import requests
from flask import render_template
from jinja2 import Template
from app import APP_NAME

REPORTING_ENDPOINT = [
    'https://reporting.elementexpress.com',
    'https://certreporting.elementexpress.com'
]

XML_NAMESPACE_REPORTING = 'https://reporting.elementexpress.com'
XML_NAMESPACE_TRANSACTION = 'https://transaction.elementexpress.com'

TRANSACTION_TICKET_NUMBER = 100000

LOGGER = logging.getLogger(APP_NAME)


def make_request(request_data, url):
    try:
        print(request_data)
        response = requests.post(
            url,
            headers={"content-type": 'text/xml;charset="utf-8"', "accept": "text/xml"},
            data=request_data
        )
        LOGGER.info(u'Transaction response: {}'.format(response.text))
    except Exception as e:
        LOGGER.warning(u'Transaction setup uncaught exception: {}'.format(traceback.format_exc()))
        raise Exception

    if response.status_code != requests.codes.ok:
        LOGGER.warning(u'Transaction setup failed: {} {}'.format(
            response.status_code,
            response.text
        ))
        raise Exception(response.status_code, response.text)
    return response


def start_payment(self, params):
    start_request_data_tmp = Template(open(f'{os.path.dirname(__file__)}/transaction_setup.xml', 'r').read())
    context = {
        'namespace': XML_NAMESPACE_TRANSACTION,
        'account_id': self.get('account_id'),
        'account_token': self.get('account_token'),
        'acceptor_id': self.get('acceptor_id'),
        'application_id': self.get('application_id'),
        'transaction_amount': '{:.2f}'.format(params.get('grand_total')),
        'return_url': params.get('return_url'),
        'reference_number': str(params.get('order_id')),
        'ticket_number': TRANSACTION_TICKET_NUMBER,
    }
    start_request_data = start_request_data_tmp.render(
        **context
    )
    response = make_request(start_request_data, self.get('transaction_url'))
    setup_response = response

    return setup_response.transaction_setup_id


def capture_transaction(self, params):
    start_request_data_tmp = Template(open(f'{os.path.dirname(__file__)}/transaction_capture.xml', 'r').read())
    start_request_data = start_request_data_tmp.render(
        namespace=XML_NAMESPACE_TRANSACTION,
        account_id=self.account_id,
        account_token=self.account_token,
        acceptor_id=self.acceptor_id,
        application_id=self.application_id,
        transaction_amount=params.grand_total,
        reference_number=str(params.order_id),
        ticket_number=TRANSACTION_TICKET_NUMBER,
        transaction_id=params.transaction_id
    )

    response = self.make_request(start_request_data, self.transaction_url)
    capture_response = response
    return capture_response


def get_transaction_status(self, transaction_setup_id):

    transaction_query_data_tmp = Template(open(f'{os.path.dirname(__file__)}/transaction_query.xml', 'r').read())
    transaction_query_data = transaction_query_data_tmp.render(
        namespace=XML_NAMESPACE_REPORTING,
        account_id=self.account_id,
        account_token=self.account_token,
        acceptor_id=self.acceptor_id,
        application_id=self.application_id,
        transaction_setup_id=transaction_setup_id,
    )

    response = make_request(transaction_query_data, REPORTING_ENDPOINT[int(self.test_mode)])
    transaction_status = response

    return transaction_status


def void_preauthorized_transaction(self, params):
    transaction_query_data_tmp = Template(open(f'{os.path.dirname(__file__)}/transaction_void.xml', 'r').read())
    transaction_query_data = transaction_query_data_tmp.render(
        namespace=XML_NAMESPACE_TRANSACTION,
        account_id=self.account_id,
        account_token=self.account_token,
        acceptor_id=self.acceptor_id,
        application_id=self.application_id,
        transaction_setup_id=params.transaction_setup_id,
        transaction_id=params.transaction_id,
        transaction_amount=params.transaction_amount,
        reference_number=params.reference_number,
        ticket_number=TRANSACTION_TICKET_NUMBER
    )
    if not params.is_voided:
        make_request(transaction_query_data, self.transaction_url)
        transaction_status = self.get_transaction_status(params.transaction_setup_id)
    else:
        transaction_status = params

    return transaction_status
